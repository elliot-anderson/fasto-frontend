import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import UserAvatar from "react-user-avatar";
import axios from "axios";
import "./Home.css";
const style = {
  flexGrow: 1,
  marginLeft: "75px",
  fontWeight: "bold",
};
axios
  .get("https://icanhazip.com")
  .then((ip) => localStorage.setItem("ip", ip.data));
const remoteIp = localStorage.getItem("ip");

const Home = ({ signOut, username, mans, plas }) => {
  return (
    <div>
      <AppBar position="static" style={{ backgroundColor: "rgb(22,32,46)" }}>
        <Toolbar>
          <Typography variant="h6" style={style}>
            Dashboard
          </Typography>
          <div
            style={{
              backgroundColor: " rgb(22,32,46)",
              color: "white",
              fontWeight: "bold",
              border: "solid 1px rgb(22,32,46)",
              width: "250px",
              margin: "30px",
            }}
          >
            Last IP address:<div style={{ float: "right" }}>{remoteIp}</div>
          </div>
          <UserAvatar
            className="UserAvatar"
            size="48"
            name="Admin"
            src="https://www.aldiwanonline.com/turkish/wp-content/uploads/avatars/1/361c53f3af227096b4d9ed895085f5dd-bpfull.jpg"
          />
          <button className="homebut" style={{ textTransform: "capitalize" }}>
            {username}
          </button>

          <button onClick={signOut} className="homebut">
            Logout
          </button>
        </Toolbar>
      </AppBar>

      <div
        style={{
          width: "400px",
          height: "100px",
          backgroundColor: "white",
          color: "rgb(22,32,46)",
          transform: "translate(60%,170%)",
        }}
      >
        <i className="fa fa-suitcase fa-3x" aria-hidden="true">
          {" "}
          {mans} Managers
        </i>
      </div>
      <div
        style={{
          width: "400px",
          height: "100px",
          backgroundColor: "white",
          color: "rgb(22,32,46)",
          transform: "translate(200%,75%)",
        }}
      >
        <i className="fa fa-map-marker fa-3x" aria-hidden="true">
          {" "}
          {plas} Places
        </i>
      </div>
    </div>
  );
};

export default Home;
