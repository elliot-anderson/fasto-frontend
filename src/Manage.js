import React, { useState } from "react";
import "./Home.css";
import { Navbar, Form } from "react-bootstrap";
import styled from "styled-components";
import Modal from "react-modal";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import { useForm } from "react-hook-form";
import Editor from "./components/Editor";
Modal.setAppElement("#root");
function Manage({
  fdata,
  addRow,
  chStatus,
  delRow,
  search,
  updateSearch,
  bsearch,
  rex,
  setRex,
}) {
  const [modalIsOpen, setIsOpen] = useState(false);
  const openModal = () => setIsOpen(true);
  const closeModal = () => setIsOpen(false);

  const { register, handleSubmit } = useForm();
  return (
    <div className="center">
      <h3 style={{ textAlign: "center", fontWeight: "bold" }}>
        Managers table
      </h3>
      <Styles>
        <Navbar expand="lg">
          <button className="btn btn-outline-primary" onClick={openModal}>
            Add New
          </button>
          <Modal
            closeTimeoutMS={500}
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            contentLabel="DMS Modal"
            style={customStyles}
          >
            <ModalBody>
              <form
                onSubmit={handleSubmit((data) =>
                  addRow(
                    data.fname ? data.fname : "awaiting",
                    data.email ? data.email : "awaiting@awaiting.com",
                    data.passwd ? data.passwd : "awaiting1235",
                    data.rname,
                    data.location
                  )
                )}
              >
                <p>
                  {" "}
                  <label>Name</label>
                </p>
                <p>
                  {" "}
                  <input
                    type="text"
                    name="fname"
                    className="form-control"
                    ref={register}
                  />{" "}
                </p>
                <p>
                  {" "}
                  <label>Email</label>
                </p>
                <p>
                  {" "}
                  <input
                    type="text"
                    className="form-control"
                    ref={register}
                    name="email"
                  />
                </p>
                <p>
                  {" "}
                  <label>Password</label>
                </p>
                <p>
                  {" "}
                  <input
                    type="password"
                    autoComplete="on"
                    className="form-control"
                    name="passwd"
                    ref={register}
                  />
                </p>
                <p>
                  {" "}
                  <label>Resturant Name</label>
                </p>
                <p>
                  {" "}
                  <input
                    type="text"
                    required
                    className="form-control"
                    name="rname"
                    ref={register}
                  />
                </p>
                <p>
                  {" "}
                  <label>Resturant Location</label>
                </p>
                <p>
                  {" "}
                  <input
                    type="text"
                    required
                    className="form-control"
                    name="location"
                    ref={register}
                  />
                </p>
                <ModalFooter>
                  <button
                    className="btn btn-outline-primary"
                    style={{ float: "right" }}
                  >
                    {" "}
                    Submit{" "}
                  </button>
                  <button
                    className="btn btn-outline-danger"
                    onClick={closeModal}
                    style={{ float: "right" }}
                  >
                    {" "}
                    Cancel{" "}
                  </button>
                </ModalFooter>
              </form>
            </ModalBody>
          </Modal>
          <Form className="form-center">
            <div className="input-group mb-0">
              {bsearch ? (
                <input
                  type="text"
                  value={search}
                  autoFocus
                  onChange={updateSearch}
                  className="form-control"
                  placeholder="Search"
                />
              ) : (
                <input
                  type="text"
                  value={search}
                  onChange={updateSearch}
                  className="form-control"
                  placeholder="Search"
                />
              )}
            </div>
          </Form>
        </Navbar>
      </Styles>

      <table id="managers" className="table">
        <tbody>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Resturant</th>
            <th>Location (latitude, longitude)</th>
            <th>Manager Status</th>
            <th style={{ textAlign: "center" }}>Operation</th>
          </tr>

          {fdata.map((item) => (
            <tr key={item ? item.id : ""}>
              <td className="tldr">
                <Editor
                  rex={rex}
                  setRex={setRex}
                  mdata={item ? item.m_name : ""}
                  id={item.id}
                />
              </td>
              <td>
                <Editor
                  rex={rex}
                  setRex={setRex}
                  ndata={item ? item.m_email : ""}
                  id={item.id}
                />
              </td>
              <td>
                <Editor
                  rex={rex}
                  setRex={setRex}
                  kdata={item ? item.dp_name : ""}
                  id={item.id}
                />
              </td>
              <td>
                <Editor
                  rex={rex}
                  setRex={setRex}
                  ldata={
                    (item ? item.latitude : "").toString() +
                    "," +
                    (item ? item.longitude : "").toString()
                  }
                  id={item.id}
                />{" "}
              </td>
              <td> {item.m_working ? "Working" : "Fired"}</td>
              <td className="operation">
                <span className="align_row">
                  <button
                    className={item.m_working ? "button" : "buttoned"}
                    onClick={() => chStatus(item.id, !item.m_working)}
                  >
                    {item.m_working ? "Fire" : "Hire"}
                  </button>
                  <button className="delete" onClick={() => delRow(item.id)}>
                    Delete
                  </button>
                </span>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
  overlay: { zIndex: 1000 },
};
const Styles = styled.div`
  a,
  .navbar-nav,
  .navbar-light .nav-link {
    &:hover {
      color: grey;
    }
  }
  .navbar-brand {
    font-size: 1.4em;

    &:hover {
      color: grey;
    }
  }
  .form-center {
    position: absolute !important;
    left: 25%;
    right: 25%;
  }
`;
export default Manage;
