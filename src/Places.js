import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import React from "react";
import "./Places.css";

function Places({ google, data }) {
  const displayMarkers = () => {
    return data.map((item) => {
      return (
        <Marker
          key={item.id}
          id={item.id}
          position={{
            lat: item.latitude,
            lng: item.longitude,
          }}
          title={item.dp_name}
          label={{
            text: item.dp_name,
            color: "black",
            pixelOffset: "0",
            fontWeight: "bold",
            fontSize: "16px",
          }}
        />
      );
    });
  };

  var x = (window.innerWidth - 75).toString() + "px";

  return (
    <div className="map">
      <Map
        style={{ height: "100%", width: x }}
        google={google}
        zoom={12}
        initialCenter={{ lat: 23.33, lng: 33.33 }}
      >
        {displayMarkers()}
      </Map>
    </div>
  );
}

export default GoogleApiWrapper({
  apiKey: process.env.API_KEY || "",
})(Places);
