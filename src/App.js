import React, { useState, useEffect } from "react";

import "./../node_modules/bootstrap/dist/css/bootstrap.min.css";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import Home from "./Home";
import Manage from "./Manage";
import Places from "./Places";
import Login from "./Login";
import Sidebar from "./components/Sidebar";
import axios from "./components/axios";
import { useForm } from "react-hook-form";
import "dotenv/config";

function App() {
  const [rex, setRex] = useState(0);
  const { register, handleSubmit } = useForm();
  const [data, setData] = useState([]);
  const signOut = () => {
    localStorage.setItem("token", "");
    setTimeout(() => window.location.replace("/login"), 100);
  };

  const token = localStorage.getItem("token");
  useEffect(() => {
    const bsearch = localStorage.getItem("bsearch");
    if (bsearch) localStorage.setItem("bsearch", "");
    if (token && window.location.pathname === "/login")
      window.location.replace("/");
    async function fetchData() {
      if (token) {
        const result = await axios.get("/doners");
        setData(result?.data);
      }
    }

    fetchData();
  }, [token, rex]);
  const onSubmit = ({ username, password }) => {
    axios
      .post("/auth/local", {
        identifier: username,
        password: password,
      })
      .then((response) => {
        localStorage.setItem("token", response.data.jwt);
        localStorage.setItem("username", response.data.user.username);
        window.location.replace("/");
      })
      .catch((error) => {
        alert(error);
      });
  };
  const [search, setSearch] = useState("");
  const bsearch = localStorage.getItem("bsearch") || "";
  const updateSearch = (event) => {
    setSearch(event.target.value.substr(0, 20));
    if (event.target.value)
      localStorage.setItem("bsearch", event.target.value.substr(0, 20));
  };

  let fdata = data.filter((item) => {
    const vals = Object.keys(item)
      .map((key) =>
        typeof item[key] === "boolean"
          ? item[key]
            ? "Working"
            : "Fired"
          : item[key]
      )
      .join(",");
    return vals.toLowerCase().indexOf(search.toLowerCase()) !== -1;
  });
  var count = 0;
  for (var i = 0; i < data.length; ++i) {
    if (data[i]["m_working"] === true) count++;
  }
  const delRow = (id) => {
    if (window.confirm("Are you sure to delete?")) {
      axios
        .delete(`/doners/${id}`)
        .then(setTimeout(() => setRex(rex + 1), 700));
    }
  };
  const RequireAuth = ({ children }) => {
    const token = localStorage.getItem("token");
    if (!token) return <Redirect to={"/login"} />;

    return children;
  };

  const chStatus = (id, status) => {
    let ans = status ? "hire" : "fire";
    if (window.confirm(`Are you sure to ${ans} ?`)) {
      axios
        .put(`/doners/${id}`, { m_working: status })
        .then(setTimeout(() => setRex(rex + 1), 700));
    }
  };
  const addRow = (fname, email, passwd, rname, location) => {
    axios
      .post(
        `/doners/`,

        {
          m_name: fname,
          m_email: email,
          mpasswd: passwd,
          dp_name: rname,
          latitude: parseFloat(location.split(",")[0]),
          longitude: parseFloat(location.split(",")[1]),
          m_working: true,
        }
      )
      .then(setTimeout(() => setRex(rex + 1), 700))
      .catch((error) => {
        alert(error + "\nEnter only valid data!");
      });
  };
  const username = localStorage.getItem("username");

  return (
    <React.Fragment>
      <Router>
        <Switch>
          <Route
            path="/login"
            component={() => (
              <Login
                handleSubmit={handleSubmit}
                onSubmit={onSubmit}
                register={register}
              />
            )}
          />
          <RequireAuth>
            <Sidebar />
            <Route
              exact
              path="/"
              component={() => (
                <Home
                  signOut={signOut}
                  username={username}
                  mans={count}
                  plas={data.length}
                />
              )}
            />
            <Route
              path="/manage"
              component={() => (
                <Manage
                  fdata={fdata}
                  addRow={addRow}
                  chStatus={chStatus}
                  rex={rex}
                  setRex={setRex}
                  delRow={delRow}
                  search={search}
                  bsearch={bsearch}
                  updateSearch={updateSearch}
                />
              )}
            />

            <Route path="/places" component={() => <Places data={data} />} />
            <Route path="/logout" component={signOut} />
          </RequireAuth>
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
