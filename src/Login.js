import React, { useState } from "react";
import loginImg from "./login.svg";
import "./loginstyles.scss";

function Login({ handleSubmit, onSubmit, register }) {
  const [isRevealedPassword, setRev] = useState(false);

  const togglePassword = () => setRev(!isRevealedPassword);
  return (
    <div className="login">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="base-container">
          <div className="header">Login</div>

          <div className="content">
            <div className="image">
              <img src={loginImg} alt="login" />
            </div>

            <div className="form">
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <input
                  ref={register}
                  required
                  type="text"
                  name="username"
                  placeholder="username"
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <span onClick={togglePassword} className="toggleme">
                  <span>
                    {isRevealedPassword ? (
                      <i className="fa fa-eye-slash " aria-hidden="true" />
                    ) : (
                      <i className="fa fa-eye " aria-hidden="true" />
                    )}
                  </span>
                </span>
                <input
                  ref={register}
                  required
                  type={isRevealedPassword ? "text" : "password"}
                  autoComplete="on"
                  name="password"
                  placeholder="password"
                />
              </div>
            </div>
          </div>
          <div className="footer">
            <button type="submit" className="btnm">
              Login
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default Login;
