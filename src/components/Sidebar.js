import React from "react";
import styled from "styled-components";
import {
  BrowserRouter as Router,
  Route,
  Link,
  withRouter,
} from "react-router-dom";

const StyledSideNav = styled.div`
  position: fixed;
  height: 100%;
  max-width: 75px;
  width: 75px;
  z-index: 1;
  background-color: rgb(23, 32, 46);
  overflow-x: hidden;
  padding-top: 20px;
  float: left;
`;

const SideNav = (props) => {
  const [state, setState] = React.useState({
    activePath: props.location.pathname,
    items: [
      {
        path: "/",
        name: "Home",
        css: "fa fa-tachometer",
        key: 1,
      },
      {
        path: "/manage",
        name: "Manage",
        css: "fas fa-suitcase",
        key: 2,
      },

      {
        path: "/places",
        name: "Places",
        css: "fas fa-map-marker-alt",
        key: 3,
      },
      {
        path: "/logout",
        name: "Logout",
        css: "fa fa-sign-out",
        key: 4,
      },
    ],
  });

  const onItemClick = (path) => {
    setState({ ...state, activePath: path });
  };

  const { items, activePath } = state;
  return (
    <StyledSideNav>
      {items.map((item) => {
        return (
          <NavItem
            path={item.path}
            name={item.name}
            css={item.css}
            onItemClick={onItemClick}
            active={item.path === activePath}
            key={item.key}
          />
        );
      })}
    </StyledSideNav>
  );
};

const RouterSideNav = withRouter(SideNav);

const StyledNavItem = styled.div`
  height: 70px;
  width: 75px;
  text-align: center;
  margin-bottom: 0;
  margin-top: 0;
  a {
    font-size: 2.7em;
    color: ${(props) => (props.active ? "white" : "rgb(130,140,157)")};
    :hover {
      opacity: 0.7;
      text-decoration: none;
    }
  }
`;

const NavItem = (props) => {
  const handleClick = () => {
    const { path, onItemClick } = props;
    onItemClick(path);
  };

  const { active } = props;
  return (
    <StyledNavItem active={active}>
      <Link to={props.path} className={props.css} onClick={handleClick}>
        <NavIcon></NavIcon>
      </Link>
    </StyledNavItem>
  );
};

const NavIcon = styled.div``;

export default class Sidebar extends React.Component {
  render() {
    return (
      <RouterSideNav>
        <Router>
          <Route />
        </Router>
      </RouterSideNav>
    );
  }
}
