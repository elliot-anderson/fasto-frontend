import React from "react";
import axios from "./axios";

const Editor = (props) => {
  const inputText = React.useRef(null);
  const [state, setState] = React.useState({});
  React.useEffect(
    () =>
      setState({
        value: props.mdata
          ? props.mdata
          : props.ndata
          ? props.ndata
          : props.kdata
          ? props.kdata
          : props.ldata
          ? props.ldata
          : "",
        isInEditMode: false,
      }),
    [props]
  );

  const changEditMode = () => {
    setState({
      ...state,
      isInEditMode: !state.isInEditMode,
    });
  };

  const updateComponentValue = () => {
    setState({
      ...state,
      isInEditMode: false,
    });
    var params = {};
    if (props.mdata) params = { m_name: inputText.current?.value };
    if (props.ndata) params = { m_email: inputText.current?.value };
    if (props.kdata) params = { dp_name: inputText.current?.value };
    if (props.ldata)
      params = {
        latitude: inputText.current?.value.split(",")[0],
        longitude: inputText.current?.value.split(",")[1],
      };
    axios
      .put(`/doners/${props.id}`, params)
      .then(setTimeout(() => props.setRex(props.rex + 1), 700))
      .catch((error) => alert(error));
  };

  const renderEditView = () => {
    return (
      <div className="editedMorder">
        <input
          type="text"
          className="form-control"
          ref={inputText}
          defaultValue={state.value}
        />
        <span
          onClick={updateComponentValue}
          role="img"
          className="btn btn-default btn-sm"
          aria-labelledby="save"
        >
          ✔️
        </span>
        <span style={{ paddingRight: "6px" }} />
        <span
          onClick={changEditMode}
          role="img"
          className="btn btn-default btn-sm "
          aria-labelledby="cancel"
        >
          ✖️
        </span>
      </div>
    );
  };
  const renderDefaultView = () => (
    <div onDoubleClick={changEditMode} title={"Double click to edit!"}>
      {state.value}
    </div>
  );

  return state.isInEditMode ? renderEditView() : renderDefaultView();
};
export default Editor;
