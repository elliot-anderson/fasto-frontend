import axios from "axios";
const token = localStorage.getItem("token");
const headers = token
  ? {
      Authorization: `Bearer ${token}`,
    }
  : null;
const instance = axios.create({
  baseURL: process.env.BASE_URL || "http://localhost:1337",
  headers: headers,
});
export default instance;
